<?php 
// Paging, encryption and token module should be implements in future

// include database and object files
include_once 'Config/Database.php';
include_once 'Classes/Product.php';
include_once 'Classes/Category.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// pass connection to objects
$product = new Product($db);
$category = new Category($db);



// import functionality, We need to add transaction with rollback, so if any issue with the json
//	like product already exist, any data missing, whole json file will not uploaded	
// Read the JSON file 
$fileData = file_get_contents('Seed Data.json');
  
// Decode the JSON file
$json_data = json_decode($fileData,true);
  
// Display data
foreach($json_data['products'] as $items){
	$tempCat = [];
	$categoryArray = explode(',', $items['category']);
	foreach($categoryArray as $value){
		$tempCat[] = $category->checkOrInsertCategory(trim($value));
	}
	// set product property values
    $product->name = $items['name'];
    $product->price = $items['price'];
    $product->sku = $items['sku'];
    $product->category = $tempCat;
  
    // create the product
    $product->create();
}	



// query products
$stmt = $product->productList();
$num = $stmt->rowCount();

// set page headers
$page_title = "Product Listing";
include_once "layout/header.php";

?>

<div class='right-button-margin'>
    <a href='create_product.php' class='btn btn-default pull-right'>Create Product</a>
</div>
<?php  
// display the products if there are any
if($num>0){?>

    <table class='table table-hover table-responsive table-bordered'>
        <tr>
            <th>Product</th>
            <th>Price</th>
            <th>SKU</th>
            <th>Category</th>
            <th>Actions</th>
        </tr>
<?php  
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){  ?>              
            <tr>
                <td><?php echo $row['name'];?></td>
                <td><?php echo $row['price'];?></td>
                <td><?php echo $row['sku'];?></td>
                <td><?php
                    $categoryArray = json_decode($row['category'], true);
					if(is_array($categoryArray) && count($categoryArray)>0){						
						foreach($categoryArray as $obj){
							$category->id = $obj;
							$category->getCategoryName();
							echo $category->name; echo '<br>';
						}
					}?>
                </td>
  
                <td>
                    <a href="read_product.php?id=<?php echo $row['id'];?>" class="btn btn-primary left-margin">
						<span class="glyphicon glyphicon-list"></span> Read
					</a>
					  
					<a href="update_product.php?id=<?php echo $row['id'];?>" class="btn btn-info left-margin">
						<span class="glyphicon glyphicon-edit"></span> Edit
					</a>
					  
					<a delete-id="<?php echo $row['id'];?>" class="btn btn-danger delete-object">
						<span class="glyphicon glyphicon-remove"></span> Delete
					</a>
					
                </td>  
            </tr><?php  
        }?>  
    </table>
	<?php
}  
// tell the user there are no products
else{ ?>
    <div class='alert alert-info'>No products found.</div>
<?php
}

// footer
include_once "layout/footer.php";
?>