<?php
class Product
{

    // database connection and table name
    private $conn;
    private $table_name = "products";

    // object properties
    public $id;
    public $name;
    public $price;
    public $sku;
    public $category;
    public $timestamp;

	// Pass database connection object when class object creation
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // create product
    public function create()
    {

        //write query
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    name=:name, price=:price, sku=:sku, category=:category, created=:created";

        $stmt = $this
            ->conn
            ->prepare($query);

        // posted values
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->price = floatval(strip_tags($this->price));
        $this->sku = htmlspecialchars(strip_tags($this->sku));
        $this->category = json_encode($this->category);//one product come in multiple category, you will get an array
        // to get time-stamp for 'created' field
        $this->timestamp = date('Y-m-d H:i:s');

        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":category", $this->category);
        $stmt->bindParam(":created", $this->timestamp);

        if ($stmt->execute())
        {
            return true;
        }
        else
        {
            return false;
        }

    }
	
	// update product
    public function update(){
  
		$query = "UPDATE
					" . $this->table_name . "
				SET
					name = :name,
					price = :price,
					category  = :category
				WHERE
					id = :id";
	  
		$stmt = $this->conn->prepare($query);
	  
		// posted values
		$this->name = htmlspecialchars(strip_tags($this->name));
		$this->price = floatval(strip_tags($this->price));
		$this->category = json_encode($this->category); // if in case product have multiple category, need to put with comma or a relation table
		$this->id = intval(strip_tags($this->id));
	  
		// bind parameters
		$stmt->bindParam(':name', $this->name);
		$stmt->bindParam(':price', $this->price);
		$stmt->bindParam(':category', $this->category);
		$stmt->bindParam(':id', $this->id);
	  
		// execute the query
		if($stmt->execute()){
			return true;
		}
	  
		return false;		  
	}

	
	// get all product
	// if needed paggin, can pass no of records on 1 page and page no as param and use LIMIT
	public function productList(){
  
		$query = "SELECT
					id, `name`, sku, price, category
				FROM
					" . $this->table_name . "
				ORDER BY
					name ASC";
	  
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
	  
		return $stmt;
	}
	
	// Read product by product id
	public function productDetail(){
  
		$query = "SELECT
					name, price, sku, category
				FROM
					" . $this->table_name . "
				WHERE
					id = ?
				LIMIT
					0,1";
	  
		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->id);
		$stmt->execute();
	  
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->name = $row['name'];
		$this->price = $row['price'];
		$this->sku = $row['sku'];
		$this->category = json_decode($row['category'], true);
	}
	
	// delete the product
	public function delete(){
	  
		$query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
		  
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->id);
	  
		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
}
?>
