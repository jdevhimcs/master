<?php 
// include database and object files
include_once 'Config/Database.php';
include_once 'Classes/Product.php';
include_once 'Classes/Category.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// pass connection to objects
$product = new Product($db);
$category = new Category($db);

// set page headers
$page_title = "Create Product";
include_once "layout/header.php";

// if the form was submitted
if($_POST){
  
    // set product property values
    $product->name = $_POST['name'];
    $product->price = $_POST['price'];
    $product->sku = $_POST['sku'];
    $product->category = $_POST['category'];
  
    // create the product
    if($product->create()){
        echo "<div class='alert alert-success'>Product was created.</div>";
    }
  
    // if unable to create the product, tell the user
    else{
        echo "<div class='alert alert-danger'>Unable to create product.</div>";
    }
}  
// contents will be here
?>
<div class='right-button-margin'>
        <a href='index.php' class='btn btn-default pull-right'>Product List</a>
    </div>

  
<!-- HTML form for creating a product -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
  
    <table class='table table-hover table-responsive table-bordered'>
  
        <tr>
            <td>Name</td>
            <td><input type='text' name='name' class='form-control' ></td>
        </tr>
  
        <tr>
            <td>Price</td>
            <td><input type='text' name='price' class='form-control' ></td>
        </tr>
  
        <tr>
            <td>SKU</td>
            <td><input type='text' name='sku' class='form-control' ></td>
        </tr>
  
        <tr>
            <td>Category</td>
            <td>
            <?php
			// read the product categories from the database
			$stmt = $category->categoryList(); ?>
			<select class='form-control' name='category[]' multiple>
				<option>Select category...</option>
			<?php 
			while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)){
				echo "<option value='".$row_category['id']."'>".$row_category['name']."</option>";
			} ?>
			</select>
            </td>
        </tr>
  
        <tr>
            <td></td>
            <td>
                <button type="submit" class="btn btn-primary">Create</button>
            </td>
        </tr>
  
    </table>
</form>

<?php
  
// footer
include_once "layout/footer.php";
?>