<?php
class Database{
   
    // specify database credentials
    private $host       = "localhost";
    private $db_name    = "db_demo_ecom";
    private $username   = "root";
    private $password   = "";
    public $conn;
   
    // get the database connection
    public function getConnection(){
   
        $this->conn = null;   // default value set
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }   
        return $this->conn;
    }
}
?>