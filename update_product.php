<?php 
// get ID of the product to be edited
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');

// include database and object files
include_once 'Config/Database.php';
include_once 'Classes/Product.php';
include_once 'Classes/Category.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// pass connection to objects
$product = new Product($db);
$category = new Category($db);

// set ID property of product to be edited
$product->id = $id;

// set page headers
$page_title = "Update Product";
include_once "layout/header.php";
// if the form was submitted
if($_POST){
  
    // set product property values
    $product->name = $_POST['name'];
    $product->price = $_POST['price'];
    $product->category = $_POST['category'];
  
    // update the product
    if($product->update()){
        echo "<div class='alert alert-success alert-dismissable'>";
            echo "Product was updated.";
        echo "</div>";
    }
  
    // if unable to update the product, tell the user
    else{
        echo "<div class='alert alert-danger alert-dismissable'>";
            echo "Unable to update product.";
        echo "</div>";
    }
}  
  
// read the details of product to be edited
$product->productDetail();

// contents will be here
?>
<div class='right-button-margin'>
        <a href='index.php' class='btn btn-default pull-right'>Product List</a>
    </div>

  
<!-- post code will be here -->
  
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]."?id=".$product->id);?>" method="post">
    <table class='table table-hover table-responsive table-bordered'>
  
        <tr>
            <td>Name</td>
            <td><input type='text' name='name' value='<?php echo $product->name; ?>' class='form-control' ></td>
        </tr>
  
        <tr>
            <td>Price</td>
            <td><input type='text' name='price' value='<?php echo $product->price; ?>' class='form-control' ></td>
        </tr>
  
        <tr>
            <td>Category</td>
            <td>
                <?php
				$stmt = $category->categoryList();
				  
				// put them in a select drop-down
				echo "<select class='form-control' name='category[]' multiple>";
				  
					echo "<option>Please select...</option>";
					while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)){
						$category_id=$row_category['id'];
						$category_name = $row_category['name'];
				  
						// current category of the product must be selected
						if(in_array($category_id, $product->category)){
							echo "<option value='$category_id' selected>";
						}else{
							echo "<option value='$category_id'>";
						}
				  
						echo "$category_name</option>";
					}
				echo "</select>";
				?>
            </td>
        </tr>
  
        <tr>
            <td></td>
            <td>
                <button type="submit" class="btn btn-primary">Update</button>
            </td>
        </tr>
  
    </table>
</form>
<?php
  
// footer
include_once "layout/footer.php";
?>