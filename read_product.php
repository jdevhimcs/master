<?php 
// get ID of the product to be edited
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');

// include database and object files
include_once 'Config/Database.php';
include_once 'Classes/Product.php';
include_once 'Classes/Category.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// pass connection to objects
$product = new Product($db);
$category = new Category($db);

// set ID property of product to be edited
$product->id = $id;
  
// read the details of product to be edited
$product->productDetail();

// set page headers
$page_title = "Product View";
include_once "layout/header.php";
?>

<div class='right-button-margin'>
    <a href='index.php' class='btn btn-default pull-right'>Product Listing</a>
</div>
// HTML table for displaying a product details
<table class='table table-hover table-responsive table-bordered'>";
  
    <tr>
        <td>Name</td>
        <td><?php echo $product->name;?></td>
    </tr>
  
    <tr>
        <td>Price</td>
        <td>$<?php echo $product->price; ?></td>
    </tr>
  
    <tr>
        <td>Sku</td>
        <td><?php echo $product->sku; ?></td>
    </tr>
  
    <tr>
        <td>Category</td>
        <td><?php 
            // display category name
			$categoryArray = $product->category;
			if(is_array($categoryArray) && count($categoryArray)>0){						
				foreach($categoryArray as $obj){
					$category->id=$obj;
					$category->getCategoryName();
					echo $category->name; echo '<br>';
				}
			}
							
             ?>
        </td>
    </tr>
  
</table>
<?php
// footer
include_once "layout/footer.php";
?>