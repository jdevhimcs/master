<?php
trait Helper{
   
    // specify Some verible used in this class
    private $date_formate = 'd-M-Y';
	private $date_time_formate = 'd-M-Y H:i:s';
   
    // specify Some global functions will used in system
    public function getGlobalDateFormate($date_string){
		return date($this->date_formate, strtotime($date_string));
    }
	
	public function getGlobalDateTimeFormate($date_string){
		return date($this->date_time_formate, strtotime($date_string));
    }
}
?>