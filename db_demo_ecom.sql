-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2021 at 06:02 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_demo_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Ethiopia', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(2, 'Meat', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(3, 'Beef', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(4, 'Chili pepper', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(5, 'China', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(6, 'Fish', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(7, 'Tofu', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(8, 'Sichuan pepper', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(9, 'Peru', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(10, 'Potato', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(11, 'Yellow Chili pepper', '2021-08-15 06:01:25', '2021-08-15 04:01:25');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `sku` text NOT NULL,
  `price` int(11) NOT NULL,
  `category` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`category`)),
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `sku`, `price`, `category`, `created`, `modified`) VALUES
(1, 'Sik Sik Wat', 'DISH999ABCD', 13, '[\"1\",\"2\",\"3\",\"4\"]', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(2, 'Huo Guo', 'DISH234ZFDR', 12, '[\"5\",\"2\",\"3\",\"6\",\"7\",\"8\"]', '2021-08-15 06:01:25', '2021-08-15 04:01:25'),
(3, 'Cau-Cau', 'DISH775TGHY', 15, '[\"9\",\"10\",\"11\"]', '2021-08-15 06:01:25', '2021-08-15 04:01:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
